# Pokemon Card

A pokemon card component.

Give this component a `name` of a pokemon and it will render a pokemon card for that pokemon.

Currently supports only Gen 1 pokemon.
